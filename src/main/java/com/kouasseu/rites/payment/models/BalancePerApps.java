package com.kouasseu.rites.payment.models;

import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 17:53<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.models<br></br>
 */
public record BalancePerApps(String application,
                             List<BalancePerCountry> balancePerCountries,
                             List<WalletInfos> walletOperation,
                             List<WalletInfos> walletRefund) {
}
