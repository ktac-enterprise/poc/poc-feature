package com.kouasseu.rites.transaction.models;

import java.util.HashMap;
import java.util.Map;

import static com.kouasseu.rites.transaction.models.CurrencyEnum.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 22:24<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.models<br></br>
 */
public record CurrencyExchange(CurrencyEnum devise, CurrencyEnum convertTo) {
    public static Map<CurrencyExchange, Double> getCurrencyExchangeRate() {
       return new HashMap<>(Map.of(
                new CurrencyExchange(MUR, MUR), 1.00,
                new CurrencyExchange(MUR, XAF), 13.00,
               new CurrencyExchange(MUR, EU), 1/50.80,
                new CurrencyExchange(XAF, XAF), 1.00,
                new CurrencyExchange(XAF, MUR), 1/13.00,
               new CurrencyExchange(XAF, EU), 1/655.00,
                new CurrencyExchange(EU, EU), 1.00,
                new CurrencyExchange(EU, XAF), 655.00,
                new CurrencyExchange(EU, MUR), 50.80
        ));
    }


    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private CurrencyEnum devise, convertTo;

        public Builder convertFrom(CurrencyEnum devise) {
            this.devise = devise;
            return this;
        }

        public Builder convertTo(CurrencyEnum devise) {
            this.convertTo = devise;
            return this;
        }

        public CurrencyExchange build() {
            return new CurrencyExchange(devise, convertTo);
        }
    }

    public static final class CurrencyExchangeBuilder {
        private CurrencyEnum devise;
        private CurrencyEnum convertTo;

        private CurrencyExchangeBuilder() {
        }

        public static CurrencyExchangeBuilder aCurrencyExchange() {
            return new CurrencyExchangeBuilder();
        }

        public CurrencyExchangeBuilder withDevise(CurrencyEnum devise) {
            this.devise = devise;
            return this;
        }

        public CurrencyExchangeBuilder withConvertTo(CurrencyEnum convertTo) {
            this.convertTo = convertTo;
            return this;
        }

        public CurrencyExchange build() {
            return new CurrencyExchange(devise, convertTo);
        }
    }
}
