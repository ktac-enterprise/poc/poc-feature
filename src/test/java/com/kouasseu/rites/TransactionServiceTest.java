package com.kouasseu.rites;

import com.kouasseu.rites.transaction.TransactionService;
import com.kouasseu.rites.transaction.models.SimulateRequest;
import com.kouasseu.rites.transaction.models.SimulateResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.kouasseu.rites.transaction.models.CurrencyEnum.MUR;
import static com.kouasseu.rites.transaction.models.CurrencyEnum.XAF;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 22:14<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites<br></br>
 */

@DisplayName("Transaction Service should")
class TransactionServiceTest {

    @DisplayName("Simulate transaction to know amount to receive")
    @Test
    void simulateTransactionToKnowAmountToReceive() {
        SimulateRequest request = new SimulateRequest("1000", MUR, XAF);

        SimulateResponse response = new TransactionService().simulate(request);

        assertNotNull(request);
        assertEquals(13000.00, response.amountToReceive().doubleValue());
        assertEquals(80.00, response.fees().doubleValue());
        assertEquals(1080.00, response.amountToPay().doubleValue());
    }

    @DisplayName("Simulate transaction to know amount to pay")
    @Test
    void simulateTransactionToKnowAmountToPay() {
        SimulateRequest request = new SimulateRequest("13000", XAF, MUR);

        SimulateResponse response = new TransactionService().simulate(request);

        assertNotNull(request);
        assertEquals(13000.00, response.amountToReceive().doubleValue());
        assertEquals(80.00, response.fees().doubleValue());
        assertEquals(1080.00, response.amountToPay().doubleValue());
    }

}