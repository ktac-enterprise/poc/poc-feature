package com.kouasseu.rites.payment.models;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 18:37<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.models<br></br>
 */
public record PayRequest(String phoneNumber, String amount, String orderId, String country, PaymentMethod paymentMethod) {

   public static Builder builder() {
       return new Builder();
   }

    public static final class Builder {
        private String phoneNumber;
        private String amount;
        private String orderId;
        private String country;
        private PaymentMethod paymentMethod;

        public Builder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder withOrderId(String orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder withCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder withPaymentMethod(PaymentMethod paymentMethod) {
            this.paymentMethod = paymentMethod;
            return this;
        }

        public PayRequest build() {
            return new PayRequest(phoneNumber, amount, orderId, country, paymentMethod);
        }
    }
}
