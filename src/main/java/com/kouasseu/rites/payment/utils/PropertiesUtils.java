package com.kouasseu.rites.payment.utils;

import com.kouasseu.rites.payment.models.MonetbilProperties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 16:59<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.utils<br></br>
 */
public class PropertiesUtils {

    public static MonetbilProperties builMonetBillProperties() {
        try (InputStream propertiesStream = PropertiesUtils.class.getResourceAsStream("/transaction.properties")){
            Properties properties = new Properties();
            properties.load(propertiesStream);

            return MonetbilProperties.builder()
                    .withBaseUrl(properties.getProperty("monetbil.base-url"))
                    .withIdKey(properties.getProperty("monetbil.sandbox.id-key"))
                    .withAppKey(properties.getProperty("monetbil.sandbox.app-key"))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Could not load monet bill properties file");
        }
    }
}
