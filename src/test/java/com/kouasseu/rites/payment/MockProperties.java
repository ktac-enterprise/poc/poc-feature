package com.kouasseu.rites.payment;

import com.kouasseu.rites.payment.models.MonetbilProperties;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 20:49<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */
public class MockProperties {

    public static MonetbilProperties builMonetBillProperties() {

        return MonetbilProperties.builder()
                .withBaseUrl("mock_base_url")
                .withIdKey("mock_id_key")
                .withAppKey("mack_app_key")
                .build();

    }
}
