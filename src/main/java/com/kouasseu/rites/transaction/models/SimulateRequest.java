package com.kouasseu.rites.transaction.models;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 22:18<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.models<br></br>
 */
public record SimulateRequest(String amount, CurrencyEnum devise, CurrencyEnum finalCurrency) {

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String amount;
        private CurrencyEnum devise;
        private CurrencyEnum finalCurrency;

        public Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder withDevise(CurrencyEnum devise) {
            this.devise = devise;
            return this;
        }

        public Builder withFinalCurrency(CurrencyEnum finalCurrency) {
            this.finalCurrency = finalCurrency;
            return this;
        }

        public SimulateRequest build() {
            return new SimulateRequest(amount, devise, finalCurrency);
        }
    }
}
