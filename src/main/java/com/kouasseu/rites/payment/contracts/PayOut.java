package com.kouasseu.rites.payment.contracts;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kouasseu.rites.payment.models.MonetbilResponse;
import com.kouasseu.rites.payment.models.PayRequest;
import com.kouasseu.rites.payment.models.PaymentResponse;

import java.net.http.HttpResponse;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 18:29<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.contracts<br></br>
 */
public interface PayOut extends PaymentCode{
    MonetbilResponse<PaymentResponse> payOutTransaction(PayRequest request);

    default MonetbilResponse<PaymentResponse> toPayOutResponse(HttpResponse<String> response, ObjectMapper OBJECT_MAPPER) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(response.body(), new TypeReference<MonetbilResponse<PaymentResponse>>() {});
    }
}
