package com.kouasseu.rites.payment;

import com.kouasseu.rites.payment.models.MonetbilResponse;
import com.kouasseu.rites.payment.models.PayRequest;
import com.kouasseu.rites.payment.models.PaymentMethod;
import com.kouasseu.rites.payment.models.PaymentResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 21:05<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */
@DisplayName("Pay out service should")
class PayOutServiceTest {

    PayOutService payOutService;

    @BeforeEach
    void setUp() {
        payOutService = new PayOutService(MockProperties.builMonetBillProperties());
    }


    @DisplayName("Perform a pay out transaction with monetbil API")
    @Test
    void payOutTransaction() {
        // Given
        PayRequest payRequest = PayRequest.builder()
                .withAmount("500")
                .withCountry("CM")
                .withOrderId("RITES006")
                .withPaymentMethod(PaymentMethod.ORANGE_MONEY)
                .withPhoneNumber("237656197707")
                .build();

        // When
        MonetbilResponse<PaymentResponse> payOutResponse = payOutService.payOutTransaction(payRequest);

        // Then
        assertAll("Pay out transaction --->>",
                () -> assertNotNull(payOutResponse),
                () -> assertEquals("REQUEST_OK", payOutResponse.status()),
                () -> assertEquals("code pour la transaction généré", payOutResponse.message()),
                () -> assertNotNull(payOutResponse.data()),
                () -> assertEquals("500.0", String.valueOf(payOutResponse.data().amount())),
                () -> assertNotNull(payOutResponse.data().id()));
    }

    @DisplayName("Retrieve auth code")
    @Test
    void retrieveTransactionCode() {
        // Given

        //When
        MonetbilResponse<String> response = payOutService.retrieveTransactionCode();

        //Then
        assertAll("Transaction code --->>",
                () -> assertNotNull(response),
                () -> assertEquals("LOGIN_SUCCESS", response.status()),
                () -> assertEquals("clé de la requête généré avec succès", response.message()),
                () -> assertNotNull(response.data()));
    }
}