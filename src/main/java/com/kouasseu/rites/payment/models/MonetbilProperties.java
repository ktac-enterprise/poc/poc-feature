package com.kouasseu.rites.payment.models;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 16:54<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.models<br></br>
 */
public record MonetbilProperties (String baseUrl, String idKey, String appKey){

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String baseUrl;
        private String idKey;
        private String appKey;

        public Builder withBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public Builder withIdKey(String idKey) {
            this.idKey = idKey;
            return this;
        }

        public Builder withAppKey(String appKey) {
            this.appKey = appKey;
            return this;
        }

        public MonetbilProperties build() {
            return new MonetbilProperties(baseUrl, idKey, appKey);
        }
    }
}
