package com.kouasseu.rites.transaction.models;

import java.math.BigDecimal;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 21:44<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.models<br></br>
 */
public record SimulateResponse(BigDecimal fees, BigDecimal amountToReceive, BigDecimal amountToPay) {


    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private BigDecimal fees;
        private BigDecimal amountToReceive;
        private BigDecimal amountToPay;

        public Builder withFees(BigDecimal fees) {
            this.fees = fees;
            return this;
        }

        public Builder withAmountToReceive(BigDecimal amountToReceive) {
            this.amountToReceive = amountToReceive;
            return this;
        }

        public Builder withAmountToPay(BigDecimal amountToPay) {
            this.amountToPay = amountToPay;
            return this;
        }

        public SimulateResponse build() {
            return new SimulateResponse(fees, amountToReceive, amountToPay);
        }
    }
}
