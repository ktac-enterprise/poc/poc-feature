package com.kouasseu.rites.payment;

import com.kouasseu.rites.payment.models.BalanceResponse;
import com.kouasseu.rites.payment.models.MonetbilResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 20:25<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */

@DisplayName("Balance service should")
class BalanceServiceTest {

    BalanceService balanceService;

    @BeforeEach
    void setup() {
        balanceService = new BalanceService(MockProperties.builMonetBillProperties());
    }


    @DisplayName("Retrieve auth code")
    @Test
    void retrieveTransactionCode() {
        // Given

        //When
        MonetbilResponse<String> response = balanceService.retrieveTransactionCode();

        //Then
        assertAll("Transaction code --->>",
                () -> assertNotNull(response),
                () -> assertEquals("LOGIN_SUCCESS", response.status()),
                () -> assertEquals("clé de la requête généré avec succès", response.message()),
                () -> assertNotNull(response.data()));
    }

    @DisplayName("Retrieve rites account balance")
    @Test
    void retrieveRitesBalance() {
        // Given

        //When
        MonetbilResponse<BalanceResponse> response = balanceService.retrieveRitesBalance();

        //Then
        assertAll("Rites Balance --->>",
                () -> assertNotNull(response),
                () -> assertEquals("GET_SUCCESS", response.status()),
                () -> assertEquals("solde totale retouné", response.message()),
                () -> assertTrue(response.data().totalBalance() != 0));
    }
}