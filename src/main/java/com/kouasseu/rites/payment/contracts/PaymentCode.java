package com.kouasseu.rites.payment.contracts;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kouasseu.rites.payment.models.MonetbilResponse;

import java.net.http.HttpResponse;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 17:46<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.contracts<br></br>
 */
public interface PaymentCode{
    MonetbilResponse<String> retrieveTransactionCode();

    default MonetbilResponse<String> toPaymentCodeResponse(HttpResponse<String> response, ObjectMapper OBJECT_MAPPER) throws JsonProcessingException {
        JavaType returnType = OBJECT_MAPPER.getTypeFactory()
                .constructType(MonetbilResponse.class);

        return OBJECT_MAPPER.readValue(response.body(), returnType);
    }
}
