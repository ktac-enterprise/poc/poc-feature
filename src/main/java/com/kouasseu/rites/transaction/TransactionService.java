package com.kouasseu.rites.transaction;

import com.kouasseu.rites.transaction.models.CurrencyEnum;
import com.kouasseu.rites.transaction.models.CurrencyExchange;
import com.kouasseu.rites.transaction.models.SimulateRequest;
import com.kouasseu.rites.transaction.models.SimulateResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 21:44<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites<br></br>
 */
public class TransactionService {


    public SimulateResponse simulate(SimulateRequest request) {

        //Step 1 - Récupèrer le montant de la devise
        BigDecimal amount = new BigDecimal(request.amount());

        // Step 2 - Convertir le montant de la devise de départ vers la devise de facturation
        BigDecimal amountInBillingCurrency = amount
                .multiply(
                        BigDecimal.valueOf(CurrencyExchange.getCurrencyExchangeRate().get(
                                CurrencyExchange.builder()
                                        .convertFrom(request.devise())
                                        .convertTo(CurrencyEnum.getBillingCurrency())
                                        .build()
                        ))).setScale(2, RoundingMode.HALF_EVEN);

        // Step 3 - Calcule de la commission
        BigDecimal fee = amountInBillingCurrency.multiply(new BigDecimal("0.08"));

        // Step 4 -Déterminer le montant du
        BigDecimal amountToPay = amountInBillingCurrency.add(fee).setScale(2, RoundingMode.HALF_EVEN);

        // Step 5 - Appliquer le taux de départ -> arrivée
        BigDecimal amountToReceive = amountInBillingCurrency.multiply(
                BigDecimal.valueOf(CurrencyExchange.getCurrencyExchangeRate().get(
                        CurrencyExchange.builder()
                                .convertFrom(CurrencyEnum.getBillingCurrency())
                                .convertTo(request.devise().equals(CurrencyEnum.getBillingCurrency()) ? request.finalCurrency() : request.devise()) //
                                // TODO condition l'utilisation de la devise de final par le fait que la devise de départ soit égale à la devise de facturation sinon appliqué la devise d'arrivé
                                .build()
                ))).setScale(2, RoundingMode.HALF_EVEN);

        // Step 6 - Retourner le résultat
        return SimulateResponse.builder()
                .withAmountToPay(amountToPay)
                .withFees(fee)
                .withAmountToReceive(amountToReceive)
                .build();
    }
}
