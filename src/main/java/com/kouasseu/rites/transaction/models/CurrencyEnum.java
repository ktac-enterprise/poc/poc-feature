package com.kouasseu.rites.transaction.models;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 22:19<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.models<br></br>
 */
public enum CurrencyEnum {
    MUR,
    XAF,
    EU,
    USD,
    CAD;

    public static CurrencyEnum getBillingCurrency() {
        return MUR;
    }
}
