package com.kouasseu.rites.payment;

import com.kouasseu.rites.payment.models.MonetbilResponse;
import com.kouasseu.rites.payment.models.PayRequest;
import com.kouasseu.rites.payment.models.PaymentMethod;
import com.kouasseu.rites.payment.models.PaymentResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 20:48<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */
@DisplayName("Pay in service should")
class PayInServiceTest {

    PayInService payInService;

    @BeforeEach
    void setUp() {
        payInService = new PayInService(MockProperties.builMonetBillProperties());
    }

    @DisplayName("Check the status of a given transaction")
    @Test
    void payInStatusCheck() {

        // Given

        // When
        MonetbilResponse<String> statusResponse = payInService.payInStatusCheck("64cce8404a39c018ff7f6ee6");

        // Then
        assertAll("Pay status checking --->>",
                () -> assertNotNull(statusResponse),
                () -> assertEquals("REQUEST_OK", statusResponse.status()),
                () -> assertEquals("Status de la transaction: SUCCESS", statusResponse.message()),
                () -> assertEquals("SUCCESS" ,statusResponse.data()));
    }

    @DisplayName("Perform a pay in transaction with monetbill api")
    @Test
    void payInTransaction() {
        // Given
        PayRequest payRequest = PayRequest.builder()
                .withAmount("500")
                .withCountry("CM")
                .withOrderId("RITES006")
                .withPaymentMethod(PaymentMethod.ORANGE_MONEY)
                .withPhoneNumber("237656197707")
                .build();

        // When
        MonetbilResponse<PaymentResponse> payInResponse = payInService.payInTransaction(payRequest);

        // Then
        assertAll("Pay in transaction --->>",
                () -> assertNotNull(payInResponse),
                () -> assertEquals("REQUEST_OK", payInResponse.status()),
                () -> assertEquals("paiement en cours de traitement", payInResponse.message()),
                () -> assertNotNull(payInResponse.data()),
                () -> assertEquals("500.0", String.valueOf(payInResponse.data().amount())),
                () -> assertNotNull(payInResponse.data().id()));
    }

}