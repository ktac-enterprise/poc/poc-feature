package com.kouasseu;

import com.kouasseu.rites.payment.BalanceService;
import com.kouasseu.rites.payment.PayInService;
import com.kouasseu.rites.payment.PayOutService;
import com.kouasseu.rites.payment.contracts.Balance;
import com.kouasseu.rites.payment.contracts.PayIn;
import com.kouasseu.rites.payment.contracts.PayOut;
import com.kouasseu.rites.payment.models.*;
import com.kouasseu.rites.payment.utils.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  16/07/2023 -- 21:43<br></br>
 * By : @author alexk<br></br>
 * Project : Default (Template) Project<br></br>
 * Package : com.kouasseu<br></br>
 */

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) {

        // Retrieve Balance
        Balance balance = new BalanceService(PropertiesUtils.builMonetBillProperties());

        MonetbilResponse<BalanceResponse> response = balance.retrieveRitesBalance();

        LOGGER.info("Balance --->> {}", response.data());

        // Pay in
        PayIn payIn = new PayInService(PropertiesUtils.builMonetBillProperties());

        MonetbilResponse<PaymentResponse> payInResponse = payIn.payInTransaction(
                PayRequest.builder()
                        .withAmount("500")
                        .withCountry("CM")
                        .withOrderId("RITES006")
                        .withPaymentMethod(PaymentMethod.ORANGE_MONEY)
                        .withPhoneNumber("237656197707")
                        .build()
        );

        LOGGER.info("Pay In Response --->> {}", payInResponse.data());

        String id = payInResponse.data().id();

        LOGGER.info("Id ---->>> {}", id);

        MonetbilResponse<String> payInStatusCheck = payIn.payInStatusCheck(id);

        LOGGER.info("Pay in status --->> {}", payInStatusCheck.data());


        // Retrieve Balance

        MonetbilResponse<BalanceResponse> newBalance = balance.retrieveRitesBalance();

        LOGGER.info("New Balance --->> {}", newBalance.data());

        // Pay out
        PayOut payOut = new PayOutService(PropertiesUtils.builMonetBillProperties());

        MonetbilResponse<PaymentResponse> payOutResponse = payOut.payOutTransaction(
                PayRequest.builder()
                        .withAmount("500")
                        .withCountry("CM")
                        .withOrderId("RITES006")
                        .withPaymentMethod(PaymentMethod.ORANGE_MONEY)
                        .withPhoneNumber("237656197707")
                        .build()
        );

        LOGGER.info("Pay Out Response --->> {}", payOutResponse.data());



        // Retrieve Balance
        MonetbilResponse<BalanceResponse> newBalance2 = balance.retrieveRitesBalance();

        LOGGER.info("New Balance --->> {}", newBalance2.data());
    }
}