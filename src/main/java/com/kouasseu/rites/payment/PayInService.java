package com.kouasseu.rites.payment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kouasseu.rites.payment.contracts.PayIn;
import com.kouasseu.rites.payment.models.MonetbilProperties;
import com.kouasseu.rites.payment.models.MonetbilResponse;
import com.kouasseu.rites.payment.models.PayRequest;
import com.kouasseu.rites.payment.models.PaymentResponse;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static com.kouasseu.rites.payment.utils.BodyBuilderUtils.buildPaymentBody;
import static com.kouasseu.rites.payment.utils.BodyBuilderUtils.buildStatusBody;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 18:47<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */
public class PayInService implements PayIn {

    private final MonetbilProperties properties;

    public PayInService(MonetbilProperties properties) {
        this.properties = properties;
    }

    private static final HttpClient CLIENT = HttpClient
            .newBuilder()
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .build();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Override
    public MonetbilResponse<String> payInStatusCheck(String transactionId) {
        StringBuilder url = new StringBuilder(properties.baseUrl())
                .append("/lapas-on-trans/trans/301/status-request?");
        HttpRequest request = HttpRequest
                .newBuilder()
                .uri(URI.create(url.toString()))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(buildStatusBody(transactionId)))
                .build();
        try {
            HttpResponse<String> response = CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            return switch (response.statusCode()) {
                case 200 -> toPayInStatus(response, OBJECT_MAPPER);
                case 400 -> MonetbilResponse.<String>builder()
                        .withStatus("400")
                        .withMessage("Erreur 400")
                        .withData(null)
                        .build();
                default -> throw new RuntimeException("Monetbill API call with status code : " + response.statusCode());
            };
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Could not call Monetbill API", e);
        }
    }


    @Override
    public MonetbilResponse<PaymentResponse> payInTransaction(PayRequest request) {
        StringBuilder url = new StringBuilder(properties.baseUrl())
                .append("/lapas-on-trans/trans/301/api-payin-request?");
        HttpRequest httpRequest = HttpRequest
                .newBuilder()
                .uri(URI.create(url.toString()))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(buildPaymentBody(request, properties)))
                .build();
        try {
            HttpResponse<String> response = CLIENT.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return switch (response.statusCode()) {
                case 200 -> toPayInResponse(response, OBJECT_MAPPER);
                case 400 -> MonetbilResponse.<PaymentResponse>builder()
                        .withStatus("400")
                        .withMessage("Errur 400")
                        .withData(null)
                        .build();
                default -> throw new RuntimeException("Monetbill API call with status code : " + response.statusCode());
            };
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Could not call Monetbill API", e);
        }
    }

}
