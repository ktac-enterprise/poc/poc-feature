package com.kouasseu.rites.payment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kouasseu.rites.payment.contracts.PayOut;
import com.kouasseu.rites.payment.models.MonetbilProperties;
import com.kouasseu.rites.payment.models.MonetbilResponse;
import com.kouasseu.rites.payment.models.PayRequest;
import com.kouasseu.rites.payment.models.PaymentResponse;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static com.kouasseu.rites.payment.utils.BodyBuilderUtils.buildCredentialBody;
import static com.kouasseu.rites.payment.utils.BodyBuilderUtils.buildPaymentBody;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 20:05<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment<br></br>
 */
public class PayOutService implements PayOut {

    private final MonetbilProperties properties;

    private static final HttpClient CLIENT = HttpClient
            .newBuilder()
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .build();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public PayOutService(MonetbilProperties properties) {
        this.properties = properties;
    }

    @Override
    public MonetbilResponse<PaymentResponse> payOutTransaction(PayRequest request) {
        StringBuilder url = new StringBuilder(properties.baseUrl())
                .append("/lapas-on-trans/trans/301/app/api-payout-request?");
        HttpRequest httpRequest = HttpRequest
                .newBuilder()
                .uri(URI.create(url.toString()))
                .header("auth-code", retrieveTransactionCode().data())
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(buildPaymentBody(request, properties)))
                .build();
        try {
            HttpResponse<String> response = CLIENT.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return switch (response.statusCode()) {
                case 200 -> toPayOutResponse(response, OBJECT_MAPPER);
                case 400 -> MonetbilResponse.<PaymentResponse>builder()
                        .withStatus("400")
                        .withMessage("Errur 400")
                        .withData(null)
                        .build();
                default -> throw new RuntimeException("Monetbill API call with status code : " + response.statusCode());
            };
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Could not call Monetbill API", e);
        }
    }

    @Override
    public MonetbilResponse<String> retrieveTransactionCode() {
        StringBuilder url = new StringBuilder(properties.baseUrl())
                .append("/lapas-on-trans/trans/auth?");
        HttpRequest request = HttpRequest
                .newBuilder()
                .uri(URI.create(url.toString()))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(buildCredentialBody(properties).toString()))
                .build();

        try {
            HttpResponse<String> response = CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            return switch (response.statusCode()) {
                case 200 -> toPaymentCodeResponse(response, OBJECT_MAPPER);
                case 400 -> MonetbilResponse.<String>builder()
                        .withStatus("400")
                        .withMessage("Erreur 400")
                        .withData(null)
                        .build();
                default -> throw new RuntimeException("Monetbill API call with status code : " + response.statusCode());
            };
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Could not call Monetbill API", e);
        }
    }
}
