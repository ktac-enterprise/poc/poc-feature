package com.kouasseu.rites.payment.models;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 17:28<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.models<br></br>
 */
public record MonetbilResponse <T> (String status, T data, String message) {

    public static <T> Builder <T> builder() {
        return new Builder<>();
    }

    public static final class Builder<T> {
        private String status;
        private T data;
        private String message;

        public Builder<T> withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder<T> withData(T data) {
            this.data = data;
            return this;
        }

        public Builder<T> withMessage(String message) {
            this.message = message;
            return this;
        }

        public MonetbilResponse<T> build() {
            return new MonetbilResponse<>(status, data, message);
        }
    }
}
