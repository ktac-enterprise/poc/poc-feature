package com.kouasseu.rites.payment.utils;

import com.kouasseu.rites.payment.models.MonetbilProperties;
import com.kouasseu.rites.payment.models.PayRequest;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 19:12<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.utils<br></br>
 */
public class BodyBuilderUtils {

    public static String buildStatusBody(String transactionId) {
        return  new StringBuilder("transaction_id=")
                .append(transactionId)
                .toString();
    }

    public static StringBuilder buildCredentialBody(MonetbilProperties properties) {
        return new StringBuilder("i_space_key=")
                .append(properties.idKey())
                .append("&")
                .append("app_space_key=")
                .append(properties.appKey());
    }

    public static String buildPaymentBody(PayRequest request, MonetbilProperties properties) {
        return buildCredentialBody(properties)
                .append("&")
                .append("phonenumber=")
                .append(request.phoneNumber())
                .append("&")
                .append("amount=")
                .append(request.amount())
                .append("&")
                .append("order_id=")
                .append(request.orderId())
                .append("&")
                .append("country=")
                .append(request.country())
                .append("&")
                .append("payment_method=")
                .append(request.paymentMethod().name())
                .toString();
    }
}
