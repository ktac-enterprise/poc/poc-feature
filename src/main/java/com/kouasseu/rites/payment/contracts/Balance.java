package com.kouasseu.rites.payment.contracts;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kouasseu.rites.payment.models.BalanceResponse;
import com.kouasseu.rites.payment.models.MonetbilResponse;

import java.net.http.HttpResponse;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  04/08/2023 -- 18:03<br></br>
 * By : @author alexk<br></br>
 * Project : poc-feature<br></br>
 * Package : com.kouasseu.rites.payment.contracts<br></br>
 */
public interface Balance extends PaymentCode{
    MonetbilResponse<BalanceResponse> retrieveRitesBalance();

    default MonetbilResponse<BalanceResponse> toBalanceResponse(HttpResponse<String> response, ObjectMapper OBJECT_MAPPER) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(response.body(), new TypeReference<MonetbilResponse<BalanceResponse>>() {});
    }
}
